﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LeetCodeProblems.TwoSum
{
    public class Solution
    {
        public int[] TwoSum(int[] nums, int target)
        {
            int[] result = new int[2];
            int index = -1;

            //for (int i = 0; i < nums.Length; i++)
            //{
            //    index = Array.IndexOf(nums, target - nums[i], i + 1);
            //    if (index != -1)
            //    {
            //        result[0] = i;
            //        result[1] = index;
            //        break;
            //    }
            //}

            Dictionary<int, int> d = new Dictionary<int, int>();
            for (int i = 0; i < nums.Length; i++)
            {
                d.Add(i, nums[i]);
            }

            for (int i = 0; i < nums.Length; i++)
            {
                if (d.ContainsValue(target - nums[i]))
                {
                    result[0] = i;
                    result[1] = d.First(v => v.Value.Equals(target - nums[i])).Key;
                    break;
                }
            }

            return result;
        }
    }
}
