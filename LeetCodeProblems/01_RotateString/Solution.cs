﻿using System;

namespace LeetCodeProblems.RotateString
{
    public class Solution
    {
        public bool RotateString(string A, string B)
        {

            if (String.IsNullOrWhiteSpace(A) || String.IsNullOrWhiteSpace(B))
                return false;

            if (A.Equals(B))
                return true;

            int index = 1;
            var indexOfFirst = A.IndexOf(B.Substring(0, index));
            var final = "";

            while (indexOfFirst != -1 && indexOfFirst != A.Length)
            {
                // takes the last chunk of the string and concatenates it with the first
                final = A.Substring(indexOfFirst) + A.Substring(0, indexOfFirst);
                
                if (B.Equals(final))
                    return true;

                indexOfFirst = A.IndexOf(B.Substring(0, index));
                index++;
            }

            return false;
        }
    }
}