﻿namespace LeetCodeProblems.AddTwoNumbers
{
    public class ListNode
    {
        public int val;
        public ListNode next;
        //public ListNode left;
        //public ListNode right;
        public ListNode(int x)
        {
            val = x;
        }

        public void Print()
        {
            if (next != null)
                next.Print();
            System.Console.Write(val);
        }

    }
}
