﻿namespace LeetCodeProblems.AddTwoNumbers
{
    public class Solution
    {
        public ListNode AddTwoNumbers(ListNode l1, ListNode l2)
        {
            var sol = BuildNumber(l1, 1) + BuildNumber(l2, 1);

            ListNode result = new ListNode(sol % 10);
            sol /= 10;

            BuildList(ref result, sol);

            return result;
        }

        private int BuildNumber(ListNode l, int len)
        {
            if (l.next != null)
                return BuildNumber(l.next, len * 10) + l.val * len;
            else
                return l.val * len;
        }

        private ListNode BuildList(ref ListNode l,int sol)
        {
            while (sol != 0)
            {
                l.next = new ListNode(sol % 10);
                return BuildList(ref l.next, sol/10);
            }
            return l;
        }

    }
}
