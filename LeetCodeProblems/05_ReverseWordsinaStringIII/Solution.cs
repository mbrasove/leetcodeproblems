﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeetCodeProblems.ReverseWordsinaStringIII
{
    public class Solution
    {
        public string ReverseWords(string s)
        {
            StringBuilder sb = new StringBuilder();

            var sa = s.Split(' ').Select(x => x.Reverse());

            foreach (var substring in sa)
            {
                sb.Append(' ' + String.Join(String.Empty, substring));
            }

            return sb.ToString().Substring(1);
        }
    }
}
