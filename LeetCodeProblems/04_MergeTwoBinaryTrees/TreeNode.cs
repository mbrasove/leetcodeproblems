﻿namespace LeetCodeProblems.MergeTwoBinaryTrees
{
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int x)
        {
            val = x;
        }

        public void Print()
        {
            if (left != null)
                left.Print();
            System.Console.Write(val);
            if (right != null)
                right.Print();
        }
    }
}
