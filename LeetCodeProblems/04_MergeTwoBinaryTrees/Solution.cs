﻿namespace LeetCodeProblems.MergeTwoBinaryTrees
{
    public class Solution
    {
        public TreeNode MergeTrees(TreeNode t1, TreeNode t2)
        {
            if (t1 == null && t2 == null)
            {
                return null;
            }

            if (t1 == null)
                return t2;

            if (t2 == null)
                return t1;

            if (t2 != null)
            {
                if (t1 != null)
                {   // add root value
                    if (t1.val.GetType() != null && t2.val.GetType() != null)
                    {
                        t1.val += t2.val;
                    }
                }
                Merge(ref t1, ref t2);
            }

            return t1;
        }
        private TreeNode Merge(ref TreeNode t1,ref TreeNode t2)
        {
            if (t2 != null)
            {
                // parse right side
                if (t2.right != null)
                {
                    if (t1.right != null)
                        t1.right.val += t2.right.val;
                    else
                        t1.right = new TreeNode(t2.right.val);

                    Merge(ref t1.right,ref t2.right);
                }

                // parse left side
                if (t2.left != null)
                {
                    if (t1.left != null)
                        t1.left.val += t2.left.val;
                    else
                        t1.left = new TreeNode(t2.left.val);

                    Merge(ref t1.left,ref t2.left);
                }
            }
            return t1;
        }
    }
}