﻿using System;
using System.Linq;

namespace LeetCodeProblems.JewelsAndStones
{
    public class Solution
    {
        public int NumJewelsInStones(string J, string S)
        {
            if (String.IsNullOrWhiteSpace(J) || String.IsNullOrWhiteSpace(S))
                return 0;

            var jewels = J.ToCharArray();

            var i = S.Where(item => jewels.Any(charToCheck => item.Equals(charToCheck))).Count();

            return i;
        }
    }
}
