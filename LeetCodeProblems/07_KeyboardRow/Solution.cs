﻿using System.Linq;

namespace LeetCodeProblems.KeyboardRow
{
    public class Solution
    {
        public string[] FindWords(string[] words)
        {
            string[] result;
            int[] strIndex = new int[words.Length];
            int total = 0;

            for (int k = 0; k < words.Length; k++)
            {
                var check = words[k].ToUpper();
                if (IsInRow(check, "EIOPQRTUWY"))
                {
                    strIndex[total++] = k;
                    continue;
                }
                else if (IsInRow(check, "ADFGHJKLS"))
                {
                    strIndex[total++] = k;
                    continue;
                }
                else if (IsInRow(check, "BCMNVXZ"))
                {
                    strIndex[total++] = k;
                }
            }

            // to remove null values from result
            if (total != words.Length)
            {
                result = new string[total];
                for (int s = 0; s < total; s++)
                {
                    result[s] = words[strIndex[s]];
                }
            }
            else
            {
                return words;
            }

            return result;

        }

        private bool IsInRow(string myString, string against)
        {
            int i = 0;

            while (i != myString.Length)
            {
                if (against.IndexOf(myString[i++]) == -1)
                    return false;
            }
            return true;
        }
    }
}

//int[] topRow = new int[] { 69, 73, 79, 80, 81, 82, 84, 85, 87, 89, 113, 119, 101, 114, 116, 121, 117, 105, 111, 112 };
//int[] middleRow = new int[] { 65, 68, 70, 71, 72, 74, 75, 76, 83, 97, 115, 100, 102, 103, 104, 106, 107, 108 };
//int[] bottomRow = new int[] { 66, 67, 77, 78, 86, 88, 90, 122, 120, 99, 118, 98, 110, 109 };

//81 + 87 + 69 + 82 + 84 + 89 + 85 + 73 + 79 + 80 = 809 // 8 down 2mid
//65 + 83 + 68 + 70 + 71 + 72 + 74 + 75 + 76 = 654  //8down 4up
//90 + 88 + 67 + 86 + 66 + 78 + 77 = 552 

//69, 73, 79, 80, 81, 82, 84, 85, 87, 89 TOP(69-89) mid-top
//65, 68, 70, 71, 72, 74, 75, 76, 83 MIDDLE(65-83) bottom-mid
//66, 67, 77, 78, 86, 88, 90 BOTTOM(66-90) envelops all
//EeIiOoPpQqRrTtUuWwYy EIOPQRTUWY
//AaDdFfGgHhJjKkLlSs ADFGHJKLS
//BbCcMmNnVvXxZz BCMNVXZ

