﻿using System.Diagnostics;
using System;

namespace LeetCodeProblems
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leet Code Problems\n");

            #region Part1
            //var JaS = new JewelsAndStones.Solution();
            //Console.WriteLine("Jewels And Stones");
            //Console.WriteLine(JaS.NumJewelsInStones("aA", "aAAbbbb"));

            //var RS = new RotateString.Solution();
            //Console.WriteLine("Rotate String");
            //Console.WriteLine(RS.RotateString("abcde", "cdeab"));
            #endregion

            #region Part2
            //Console.WriteLine("Merge Two Binary Trees");
            //var TreeOne = new MergeTwoBinaryTrees.TreeNode(1);
            //TreeOne.left = new MergeTwoBinaryTrees.TreeNode(3);
            //TreeOne.right = new MergeTwoBinaryTrees.TreeNode(2);
            //TreeOne.left.left = new MergeTwoBinaryTrees.TreeNode(5);

            //var TreeTwo = new MergeTwoBinaryTrees.TreeNode(2);
            //TreeTwo.left = new MergeTwoBinaryTrees.TreeNode(1);
            //TreeTwo.right = new MergeTwoBinaryTrees.TreeNode(3);
            //TreeTwo.left.right = new MergeTwoBinaryTrees.TreeNode(4);
            //TreeTwo.right.right = new MergeTwoBinaryTrees.TreeNode(7);

            //TreeOne.Print();
            //Console.WriteLine();
            //var m2bt = new MergeTwoBinaryTrees.Solution();
            //m2bt.MergeTrees(TreeOne, TreeTwo).Print();

            //Console.WriteLine("Reverse words in a string");
            //var rwias = new ReverseWordsinaStringIII.Solution();
            //Console.WriteLine(rwias.ReverseWords("enuk$)g*)(mex%y*ocdafa)ev%konbr(ucvu*kjh$kyz*djrk)rniq##jbqtwhu*r&q#gsd#ivkni (xkpffov(frqu)!&sf&stbw)@s! eq&tj)vguf!y$sstm^! @mx%khlj$jpqs*uxwxvgu vjmlw^ubivqyyljta%q&$f@mcvc&(owvgyehq#qph*fak tqxtey kexylyiwh%!bxpcqo@zgg&tklpw%phs)cbo@(&^^wn w*xhpxa@d!!vwavwqchbfmpl&z@$kztz#nc%@!tv$htr!&d(wbj^tcfpu!z)!hyf^&sc!c)z%bgufbj#obhlykh ih$vxc*to#*wombce*eo)pqftfps^c(&kf%clklc f&$murkgdhnos$%ovvaqc&las%r%s*x^cpqvk&xlbmxejlsyt^(ck$ @)ks$i!dotdz)skwc&s^zk!ma!z@ymd%d)(flj^)va*tr^xnjgd!x b!al&bvewa#wtr^pov v$aie%x&&bx#@mnwrvu&^v$je(#se&y)x$wmgzmi!%eixawazf%*g$obyggoybw#ygjg**u(it@^b)@raa#ua(w*wxensgd u(a%qinf#wgoxt(q!&rz)ktpuqrjswqr^kispf*gzv#nkyg icd)xfhdpwwvm@i$%&ov(xkbe)igwajs@v)nepqtbjtk $dexm*bapttglgj$azwcaobdj&$ol#jnoq(f&twe@ulovnliefy%(%uncco%z#%%&w@xanjxdd"));

            //Console.WriteLine("Keyboard row");
            //var kr = new KeyboardRow.Solution();
            //Console.WriteLine(kr.FindWords(new string[] { "Hello", "Alaska", "Dad", "Peace"}));
            #endregion

            #region Part3
            //Console.WriteLine("Two Sum");
            //var ts = new TwoSum.Solution();
            //Console.WriteLine(ts.TwoSum(new int[] { 2,7,11,15 },9));

            Console.WriteLine("AddTwoNumbers");
            //var l1 = new AddTwoNumbers.ListNode(2);
            //l1.next = new AddTwoNumbers.ListNode(4);
            //l1.next.next = new AddTwoNumbers.ListNode(3);

            //var l2 = new AddTwoNumbers.ListNode(5);
            //l2.next = new AddTwoNumbers.ListNode(6);
            //l2.next.next = new AddTwoNumbers.ListNode(4);

            var l3 = new AddTwoNumbers.ListNode(9);
            //[1,9,9,9,9,9,9,9,9,9]
            var l4 = new AddTwoNumbers.ListNode(1);
            l4.next = new AddTwoNumbers.ListNode(9);
            l4.next.next = new AddTwoNumbers.ListNode(9);
            l4.next.next.next = new AddTwoNumbers.ListNode(9);
            l4.next.next.next.next = new AddTwoNumbers.ListNode(9);
            l4.next.next.next.next.next = new AddTwoNumbers.ListNode(9);
            l4.next.next.next.next.next.next = new AddTwoNumbers.ListNode(9);
            l4.next.next.next.next.next.next.next = new AddTwoNumbers.ListNode(9);
            l4.next.next.next.next.next.next.next.next = new AddTwoNumbers.ListNode(9);

            var atn = new AddTwoNumbers.Solution();
            l4.Print();
            Console.WriteLine();
            Console.WriteLine(atn.AddTwoNumbers(l3,l4));
            #endregion

            Stopwatch sw = new Stopwatch();
            sw.Start();
            // code to test here
            sw.Stop();
            Console.WriteLine("Elapsed={0}", sw.Elapsed);

            Console.ReadKey();
        }
    }
}
