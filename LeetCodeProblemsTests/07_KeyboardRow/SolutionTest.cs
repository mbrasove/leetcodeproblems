﻿using FluentAssertions;
using LeetCodeProblems.KeyboardRow;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetCodeProblemsTests.KeyboardRow
{
     [TestClass]
     public class SolutionTest
    {
        #region Setup
        private Solution solution;

        [TestInitialize]
        public void SetUp()
        {
            solution = new Solution();
        }

        [TestCleanup]
        public void TearDown()
        {
            solution = null;
        }
        #endregion

        [TestMethod]
        public void Given_Solution_When_TestingWithSampleInput_Then_Should_GetGoodResults()
        {
            // when
            var result1 = solution.FindWords(new string[] { "Hello", "Alaska", "Dad", "Peace" });
            var result2 = solution.FindWords(new string[] { "adsdf", "sfd" });

            // then - results should be correct
            result1.Should().Equal(new string[] { "Alaska", "Dad" });
            result2.Should().Equal(new string[] { "adsdf", "sfd" });
        }
    }
}
