﻿using FluentAssertions;
using LeetCodeProblems.ReverseWordsinaStringIII;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetCodeProblemsTests.ReverseWordsinaStringIII
{
    [TestClass]
    public class SolutionTest
    {
        #region Setup
        private Solution solution;

        [TestInitialize]
        public void SetUp()
        {
            solution = new Solution();
        }

        [TestCleanup]
        public void TearDown()
        {
            solution = null;
        }
        #endregion

        [TestMethod]
        public void Given_Solution_When_TestingWithSampleInput_Then_Should_GetGoodResults()
        {
            // when

            var result1 = solution.ReverseWords("Let's take LeetCode contest");
            var result2 = solution.ReverseWords("enuk$)g*)(mex%y*ocdafa)ev%konbr(ucvu*kjh$kyz*djrk)rniq##jbqtwhu*r&q#gsd#ivkni (xkpffov(frqu)!&sf&stbw)@s! eq&tj)vguf!y$sstm^! @mx%khlj$jpqs*uxwxvgu vjmlw^ubivqyyljta%q&$f@mcvc&(owvgyehq#qph*fak tqxtey kexylyiwh%!bxpcqo@zgg&tklpw%phs)cbo@(&^^wn w*xhpxa@d!!vwavwqchbfmpl&z@$kztz#nc%@!tv$htr!&d(wbj^tcfpu!z)!hyf^&sc!c)z%bgufbj#obhlykh ih$vxc*to#*wombce*eo)pqftfps^c(&kf%clklc f&$murkgdhnos$%ovvaqc&las%r%s*x^cpqvk&xlbmxejlsyt^(ck$ @)ks$i!dotdz)skwc&s^zk!ma!z@ymd%d)(flj^)va*tr^xnjgd!x b!al&bvewa#wtr^pov v$aie%x&&bx#@mnwrvu&^v$je(#se&y)x$wmgzmi!%eixawazf%*g$obyggoybw#ygjg**u(it@^b)@raa#ua(w*wxensgd u(a%qinf#wgoxt(q!&rz)ktpuqrjswqr^kispf*gzv#nkyg icd)xfhdpwwvm@i$%&ov(xkbe)igwajs@v)nepqtbjtk $dexm*bapttglgj$azwcaobdj&$ol#jnoq(f&twe@ulovnliefy%(%uncco%z#%%&w@xanjxdd");


            // then - results should be correct
            result1.Should().Be("s'teL ekat edoCteeL tsetnoc");
            result2.Should().Be("inkvi#dsg#q&r*uhwtqbj##qinr)krjd*zyk$hjk*uvcu(rbnok%ve)afadco*y%xem()*g)$kune !s@)wbts&fs&!)uqrf(voffpkx( !^mtss$y!fugv)jt&qe ugvxwxu*sqpj$jlhk%xm@ kaf*hpq#qheygvwo(&cvcm@f$&q%atjlyyqvibu^wlmjv yetxqt nw^^&(@obc)shp%wplkt&ggz@oqcpxb!%hwiylyxek hkylhbo#jbfugb%z)c!cs&^fyh!)z!upfct^jbw(d&!rth$vt!@%cn#ztzk$@z&lpmfbhcqwvawv!!d@axphx*w clklc%fk&(c^spftfqp)oe*ecbmow*#ot*cxv$hi $kc(^tysljexmblx&kvqpc^x*s%r%sal&cqavvo%$sonhdgkrum$&f x!dgjnx^rt*av)^jlf()d%dmy@z!am!kz^s&cwks)zdtod!i$sk)@ vop^rtw#awevb&la!b dgsnexw*w(au#aar@)b^@ti(u**gjgy#wbyoggybo$g*%fzawaxie%!imzgmw$x)y&es#(ej$v^&uvrwnm@#xb&&x%eia$v gykn#vzg*fpsik^rqwsjrquptk)zr&!q(txogw#fniq%a(u ktjbtqpen)v@sjawgi)ebkx(vo&%$i@mvwwpdhfx)dci ddxjnax@w&%%#z%occnu%(%yfeilnvolu@ewt&f(qonj#lo$&jdboacwza$jglgttpab*mxed$");
        }
    }
}
