﻿using FluentAssertions;
using LeetCodeProblems.JewelsAndStones;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetCodeProblemsTests.JewelsAndStones
{
    [TestClass]
    public class SolutionTest
    {
        #region Setup
        private Solution solution;

        [TestInitialize]
        public void SetUp()
        {
            solution = new Solution();
        }

        [TestCleanup]
        public void TearDown()
        {
            solution = null;
        }
        #endregion

        [TestMethod]
        public void Given_Solution_When_TestingWithSampleInput_Then_Should_GetGoodResults()
        {
            // when
            var result1 = solution.NumJewelsInStones("aA", "aAAbbbb");
            var result2 = solution.NumJewelsInStones("z", "ZZ");

            // then - results should be correct
            result1.Should().Be(3);
            result2.Should().Be(0);
        }
    }
}
