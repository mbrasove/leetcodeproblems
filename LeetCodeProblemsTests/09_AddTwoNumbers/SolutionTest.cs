﻿using FluentAssertions;
using LeetCodeProblems.AddTwoNumbers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetCodeProblemsTests.AddTwoNumbers
{
    [TestClass]
    public class SolutionTest
    {
        #region Setup
        private Solution solution;

        [TestInitialize]
        public void SetUp()
        {
            solution = new Solution();
        }

        [TestCleanup]
        public void TearDown()
        {
            solution = null;
        }
        #endregion

        [TestMethod]
        public void Given_Solution_When_TestingWithSampleInput_Then_Should_GetGoodResults()
        {
            var l1 = new ListNode(2);
            l1.next = new ListNode(4);
            l1.next.next = new ListNode(3);

            var l2 = new ListNode(5);
            l2.next = new ListNode(6);
            l2.next.next = new ListNode(4);


            var l3 = new ListNode(9);
            //[1,9,9,9,9,9,9,9,9,9]
            var l4 = new ListNode(1);
            l4.next = new ListNode(9);
            l4.next.next = new ListNode(9);
            l4.next.next.next = new ListNode(9);
            l4.next.next.next.next = new ListNode(9);
            l4.next.next.next.next.next = new ListNode(9);
            l4.next.next.next.next.next.next = new ListNode(9);
            l4.next.next.next.next.next.next.next = new ListNode(9);
            l4.next.next.next.next.next.next.next.next = new ListNode(9);


            // when
            var result1 = solution.AddTwoNumbers(l1,l2);
            var result2 = solution.AddTwoNumbers(l3,l4);

            // then - results should be correct
            //result1.Should().Equal(new int[] { 0, 1 });
            //result2.Should().Equal(new int[] { 0, 1 });

        }
    }
}
