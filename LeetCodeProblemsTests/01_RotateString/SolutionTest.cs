﻿using FluentAssertions;
using LeetCodeProblems.RotateString;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetCodeProblemsTests.RotateString
{
    [TestClass]
    public class SolutionTest
    {
        #region Setup
        private Solution solution;

        [TestInitialize]
        public void SetUp()
        {
            solution = new Solution();
        }

        [TestCleanup]
        public void TearDown()
        {
            solution = null;
        }
        #endregion

        [TestMethod]
        public void Given_Solution_When_TestingWithSampleInput_Then_Should_GetGoodResults()
        {
            // when
            var result0 = solution.RotateString("gcmbf","fgcmb");
            var result1 = solution.RotateString("abcde", "cdeab");
            var result2 = solution.RotateString("abcde", "abced");
            var result3 = solution.RotateString("ohbrwzxvxe", "uornhegseo");
            var result4 = solution.RotateString("vcuszhlbtpmksjleuchmjffufrwpiddgyynfujnqblngzoogzg", "fufrwpiddgyynfujnqblngzoogzgvcuszhlbtpmksjleuchmjf");

            // then - results should be correct
            result0.Should().Be(true);
            result1.Should().Be(true);
            result2.Should().Be(false);
            result3.Should().Be(false);
            result4.Should().Be(true);
        }
    }
}
