﻿using FluentAssertions;
using LeetCodeProblems.TwoSum;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetCodeProblemsTests.TwoSum
{
    [TestClass]
    public class SolutionTest
    {
        #region Setup
        private Solution solution;

        [TestInitialize]
        public void SetUp()
        {
            solution = new Solution();
        }

        [TestCleanup]
        public void TearDown()
        {
            solution = null;
        }
        #endregion

        [TestMethod]
        public void Given_Solution_When_TestingWithSampleInput_Then_Should_GetGoodResults()
        {
            // when
            var result1 = solution.TwoSum(new int[] { 2, 7, 11, 15 }, 9);
            var result2 = solution.TwoSum(new int[] { 4, 5, 5, 50 }, 9);
            //var result3 = solution.TwoSum(new int[] { 5, 5, 5, 5 }, 10);
            var result4 = solution.TwoSum(new int[] { 7, 1, 199, 943, 280, 433, 485, 377, 191, 697, 371, 115, 32, 958, 326, 635, 202, 497, 327, 462, 197, 278, 60, 66, 287, 357, 954, 316, 583, 109, 968, 929, 144, 825, 636, 681, 74, 243, 487, 983, 200, 707, 722, 393, 663, 829, 601, 766, 759, 673, 239, 3 }, 4);
            var result5 = solution.TwoSum(new int[] { 15, 7, 11, 2 }, 9);
            var result6 = solution.TwoSum(new int[] { 0, 4, 3, 0 }, 0);
            var result7 = solution.TwoSum(new int[] { -1,-2,-3,-4,-5}, -8);
            var result8 = solution.TwoSum(new int[] { 7, -2, 1}, 5);
            var result9 = solution.TwoSum(new int[] { 3, 2, 4 }, 6);
            var result10 = solution.TwoSum(new int[] { 7, 10, 199, 943, 280, 433, 485, 377, 191, 697, 371, 115, 32, 958, 326, 635, 202, 497, 327, 462, 197, 278, 60, 66, 287, 357, 954, 316, 583, 109, 968, 929, 144, 825, 636, 681, 74, 243, 487, 983, 200, 707, 722, 393, 663, 829, 601, 766, 759, 673, 239, 3, 1 }, 4);


            // then - results should be correct
            result1.Should().Equal(new int[] { 0, 1 });
            result2.Should().Equal(new int[] { 0, 1 });
            //result3.Should().Equal(new int[] { 0, 1 });
            result4.Should().Equal(new int[] { 1, 51 });
            result5.Should().Equal(new int[] { 1, 3 });
            result6.Should().Equal(new int[] { 0, 3 });
            result7.Should().Equal(new int[] { 2, 4 });
            result8.Should().Equal(new int[] { 0, 1 });
            result9.Should().Equal(new int[] { 1, 2 });
            result10.Should().Equal(new int[] { 51, 52 });
        }
    }
}
