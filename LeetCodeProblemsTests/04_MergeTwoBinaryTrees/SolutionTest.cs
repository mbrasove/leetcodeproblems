﻿using FluentAssertions;
using LeetCodeProblems.MergeTwoBinaryTrees;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeetCodeProblemsTests.MergeTwoBinaryTrees
{
    [TestClass]
    public class SolutionTest
    {
        #region Setup
        private Solution solution;

        [TestInitialize]
        public void SetUp()
        {
            solution = new Solution();
        }

        [TestCleanup]
        public void TearDown()
        {
            solution = null;
        }
        #endregion

        [TestMethod]
        public void Given_Solution_When_TestingWithSampleInput_Then_Should_GetGoodResults()
        {
            // when

            #region Test1
            var TreeOne = new TreeNode(1);
            TreeOne.left = new TreeNode(3);
            TreeOne.right = new TreeNode(2);
            TreeOne.left.left = new TreeNode(5);

            var TreeTwo = new TreeNode(2);
            TreeTwo.left = new TreeNode(1);
            TreeTwo.right = new TreeNode(3);
            TreeTwo.left.right = new TreeNode(4);
            TreeTwo.right.right = new TreeNode(7);

            var TreeResult = new TreeNode(3);
            TreeResult.left = new TreeNode(4);
            TreeResult.right = new TreeNode(5);
            TreeResult.left.left = new TreeNode(5);
            TreeResult.left.right = new TreeNode(4);
            TreeResult.right.right = new TreeNode(7);

            var result1 = solution.MergeTrees(TreeOne, TreeTwo);
            #endregion

            #region Test2
            var TreeTree = new TreeNode(1);
            TreeTree.left = new TreeNode(2);
            TreeTree.left.left = new TreeNode(3);

            var TreeFour = new TreeNode(1);
            TreeFour.right = new TreeNode(2);
            TreeFour.right.right = new TreeNode(3);

            var result2 = solution.MergeTrees(TreeTree, TreeFour);
            #endregion

            // then - results should be correct
            //result1.Should().Be(TreeResult);  //fails for some reason
            result2.Should().Be(TreeTree);
        }
    }
}
